import io
def print_to_string(*args, **kwargs):
    output = io.StringIO()
    print(*args, file=output, **kwargs)
    contents = output.getvalue()
    output.close()
    return contents

import numpy as np
from fractions import Fraction
def print_equations(equations):
    temp = [[f'{n.numerator}/{n.denominator}' for n in row] for row in equations]
    print(print_to_string(np.matrix(temp)).replace('\'', ''))


def divide_array(arr, divisor):
    return [i / divisor for i in arr]

def multiply_array(arr, multiplier):
    return [i * multiplier for i in arr]

class ArraysDifferentLengthsException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

def add_arrays(arr1, arr2):
    if len(arr1) != len(arr2):
        raise ArraysDifferentLengthsException(f"Cannot add arrays of lengths {len(arr1)} and {len(arr2)}!")
    
    arr3 = []
    for i in range(len(arr1)):
        arr3.append(arr1[i] + arr2[i])
    
    return arr3

def subtract_arrays(arr1, arr2):
    if len(arr1) != len(arr2):
        raise ArraysDifferentLengthsException(f"Cannot subtract arrays of lengths {len(arr1)} and {len(arr2)}!")
    
    return add_arrays(arr1, multiply_array(arr2, -1))

def solve(equations, quiet=False):
    equations = [[Fraction(n,1) for n in row] for row in equations]
    equation_count = len(equations)
    column_height = equation_count
    rows_pivoted = 0
    current_column = -1

    # equations[row][column]

    while current_column < len(equations[0]) - 1:
        if not quiet: print_equations(equations)
    
        current_column += 1
        # column loop
        if not quiet: print(f'looking for a valid pivot element in column {current_column}, starting from row {rows_pivoted}')

        # check all elements in the column after the final row that has already been pivoted with
        for i in range(column_height - rows_pivoted):
            row_index = i + rows_pivoted
            position_string = f'column {current_column}, row {row_index}'
            
            if not quiet: print(f'checking {position_string} for a valid pivot point')
            value_at_position: Fraction = equations[row_index][current_column]
            if not quiet: print(f'the value at {position_string} is {value_at_position}')
            is_valid_pivot = value_at_position != 0
            if not is_valid_pivot:
                if not quiet: print(f'not a valid pivot point, skipping')
                continue

            # pivot is valid
            if not quiet: print(f'found a valid pivot point!')
            if not quiet: print(f'dividing row {row_index} by {value_at_position}')
            equations[row_index] = divide_array(equations[row_index], value_at_position)
            if not quiet: print(f'done. new equation matrix:')
            if not quiet: print_equations(equations)

            if not quiet: print(f'using the new pivot point')

            for row_index_2 in range(column_height):
                if row_index_2 == row_index: continue

                if not quiet: print(f'pivoting row {row_index_2}, column {current_column} towards 0')
                pre_pivot_value = equations[row_index_2][current_column]
                if not quiet: print(f'value at row {row_index_2}, column {current_column} is {pre_pivot_value}. adding {-pre_pivot_value} times row {row_index} to make it 0')
                equations[row_index_2] = add_arrays(equations[row_index_2], multiply_array(equations[row_index], -pre_pivot_value))
                if not quiet: print(f'new equation grid:')
            if not quiet: print_equations(equations)
            rows_pivoted += 1
            break
        
        if not quiet: print(f'\n\n')
        if not quiet: print_equations(equations)
    return equations


if __name__ == "__main__":
    DISABLE_INTERACTIVE = True

    if not(DISABLE_INTERACTIVE):
        equation_count = int(input("How many equations are there? "))

        print("Enter the equations one at a time, seperated by commas (,): ")
        equations = []
        for _ in range(equation_count):
            equations.append([float(s) for s in input("> ").split(',')])
    else:
        # equations = [
        #     [3,  4, 10],
        #     [2, -1,  3]
        # ]
        equations = [
            [4, 2, 1, 1],
            [9, -3, 1, 4],
            [25, 5, 1, 2]
        ]
        print_equations(equations)

    solve(equations)