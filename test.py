import unittest
import solver
from fractions import Fraction

class TestPrintToString(unittest.TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.test_string = "~#@'testing1234"

    def print_string(self):
        self.assertEqual(solver.print_to_string(self.test_string), f'{self.test_string}\n')
        self.assertEqual(solver.print_to_string(self.test_string, end=""), self.test_string)

class TestArrayManipulation(unittest.TestCase):
    def test_divide_arrays(self):
        arr = [1, 8, 45, 2, 4]
        self.assertEqual(solver.divide_array(arr, 8), [0.125, 1, 5.625, 0.25, 0.5])
    
    def test_multiply_arrays(self):
        arr = [0.125, 1, 5.625, 0.25, 0.5]
        self.assertEqual(solver.multiply_array(arr, 8), [1, 8, 45, 2, 4])
        self.assertEqual(solver.multiply_array([1, -2, 3, -4], -1),
                         [-1, 2, -3, 4])
    
    def test_add_arrays(self):
        self.assertEqual(
            solver.add_arrays([1, 2, 3, 4], [5, 6, 7, 8]),
            [6, 8, 10, 12]
        )
        self.assertEqual(
            solver.add_arrays([1, -2, 3.7, 4], [-5.5, 6, 7, -8]),
            [-4.5, 4, 10.7, -4]
        )
    
    def test_add_arrays_differing_lengths(self):
        with self.assertRaises(solver.ArraysDifferentLengthsException):
            solver.add_arrays([1, 2, 3, 4], [5, 6, 7, 8, 9])
    
    def test_subtract_arrays(self):
        self.assertEqual(
            solver.subtract_arrays([5, 7, 6, 1], [4, 3, 2, 8]),
            [1, 4, 4, -7]
        )
    
    def test_subtract_arrays_differing_lengths(self):
        with self.assertRaises(solver.ArraysDifferentLengthsException):
            solver.subtract_arrays([1, 2, 3, 4], [5, 6, 7, 8, 9])

class TestSolver(unittest.TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)

    def test_basic_solve(self):
        self.assertEqual(solver.solve(
            [
                [4, 2, 1, 1],
                [9, -3, 1, 4],
                [25, 5, 1, 2]
            ], quiet=True
        ),
            [
                # [1/1, 0/1, 0/1, 7/60],
                [Fraction(1, 1), Fraction(0, 1), Fraction(0, 1), Fraction(7, 60)],
                # [0/1, 1/1, 0/1, -29/60],
                [Fraction(0, 1), Fraction(1, 1), Fraction(0, 1), Fraction(-29, 60)],
                # [0/1, 0/1, 1/1, 3/2]
                [Fraction(0, 1), Fraction(0, 1), Fraction(1, 1), Fraction(3, 2)],
            ]
        )
        
        self.assertEqual(solver.solve(
            [
                [3, 4, 10],
                [2, -1, 3]
            ], quiet=True
        ),
            [
                [Fraction(1, 1), Fraction(0, 1), Fraction(2, 1)],
                [Fraction(0, 1), Fraction(1, 1), Fraction(1, 1)]
            ]
        )
        

if __name__ == '__main__':
    unittest.main()